#
# main.py for PythonWebScannerEpitech, made with PyCharm
#
# Made by alex
# Email: <alexandre.page@epitech.eu>
# Started on 9/10/15 11:14 PM
#

import urllib2
import urllib
import json
import subprocess

class scan(object):

    def __init__(self, login, ppp_pwd, unix_pwd):
        self.login = login
        self.ppp_pwd = ppp_pwd
        self.unix_pwd = unix_pwd

        self.port = 80
        self.timeout = str(1)
        self.promo = str(2019)
        self.city = 'strasbourg'

    def start(self):
        print "City %s" % (self.city)
        print "Promo : %s" % (self.promo)
        url = "http://ws.paysdu42.fr/JSON/?action=get_logins&auth_login=%s&auth_password=%s&school=epitech&promo=%s&city=%s" % (self.login, self.ppp_pwd, self.promo, self.city)
        response = urllib2.urlopen(url)
        html = response.read()
        data = json.loads(html)

        logins = data['result']
        for login in logins:
            self.get_ip(login)

    def get_ip(self, login):
        url = "https://intra.epitech.eu/user/%s/?format=json" % (login)
        values = {'login': self.login,
                  'password': self.unix_pwd }
        data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        html = response.read()
        my_json = json.loads(html)

        locations = my_json['locations']
        if locations:
            for location in locations:
                self.test_ip(login, location['host'])
        else:
            print "%s : not connected" % (login)

    def test_ip(self, login, ip):
        try:
            response = subprocess.check_output(
                ['ping','-c', '1', '-W', self.timeout, ip],
                stderr=subprocess.STDOUT,
            )
        except subprocess.CalledProcessError:
            response = None
        if response:
            print "%s : Web Server running -> %s" % (login, ip)
        else:
            print "%s : No Web server running" % (login)

def main():
    s = scan("", "", "")
    s.start()


if __name__ == '__main__':
    main()